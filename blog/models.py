from django.db import models

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=30)
    author = models.CharField(max_length=20)
    date = models.DateField(auto_now=False, auto_now_add=True)
    text = models.TextField()

    def __str__(self):
        return self.title
        