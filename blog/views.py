from django.shortcuts import render
from django.views.generic import TemplateView, DetailView
from blog.models import Post

# Create your views here.
class IndexView(TemplateView):
    template_name = "blog/base.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['posts'] = Post.objects.order_by('-date')
        return context

class PostDetailView(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['posts'] = Post.objects.all()
        return context